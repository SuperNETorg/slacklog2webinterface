<?php

require __DIR__ . '/../init.php';

use Illuminate\Database\Capsule\Manager as DB;

echo @date('[Y-m-d H:i]') . " fetching new messages " . str_repeat('*', 20) . PHP_EOL;

$conn = DB::connection();
$slackApi = SlackApi::init();

// Fetch users list
echo "Fetch users list" . PHP_EOL;
$usersData = $slackApi->getUsers();
$conn->beginTransaction();
foreach ($usersData as $userData)
{
    /** @var DB_User $user */
    $user = DB_User::findOrNew($userData['id']);
    $user->setRawAttributes(
        array(
            'id' => $userData['id'],
            'name' => $userData['name'],
            'deleted' => $userData['deleted'] ? 1 : 0,
            'real_name' => $userData['profile']['real_name'],
            'email' => @$userData['profile']['email'],
            'skype' => @$userData['profile']['skype'],
            'phone' => @$userData['profile']['phone'],
        )
    );
    $user->save();
}
$conn->commit();


// Fetch channel list
echo "Fetch channel list" . PHP_EOL;
$channelsData = $slackApi->getChannels();
$conn->beginTransaction();
foreach ($channelsData as $channelData)
{
    /** @var DB_Channel $channel */
    $channel = DB_Channel::findOrNew($channelData['id']);
    $channel->setRawAttributes(
        array(
            'id' => $channelData['id'],
            'name' => $channelData['name'],
        )
    );
    $channel->save();
}
$conn->commit();

// Fetch messages for every channel
echo "Fetch channel messages" . PHP_EOL;
foreach (DB_Channel::all() as $channel)
{
    echo "\tChannel #{$channel->id} '{$channel->name}'" . PHP_EOL;

    $lastMessageFetched = $channel->last_message_read;

    $fromTs = $lastMessageFetched;
    $toTs = 0;

    $firstMsgTs = null;

    $conn->beginTransaction();

    $cnt = 0;
    // Loop - channel messages
    do
    {
        $history = $slackApi->getChannelHistory($channel->id, $toTs, $fromTs);
        foreach ($history['messages'] as $messageData)
        {
            if ($firstMsgTs === null)
            {
                $firstMsgTs = $messageData['ts'];
            }

            // skip event messages
            if (isset($messageData['subtype']))
                //($messageData['subtype'] == 'bot_message' || $messageData['subtype'] == 'file_comment'))
            {
                continue;
            }

            $msg = DB_Message::create(array(
                'channel' => $channel->id,
                'text' => $messageData['text'],
                'ts' => $messageData['ts'],
                'type' => $messageData['type'],
                'user' => $messageData['user'],
            ));

            $cnt++;
            $toTs = $messageData['ts'];
        }
    }
    while ($history['has_more']);

    echo "\t\tGot $cnt new messages" . PHP_EOL;

    if ($firstMsgTs)
    {
        $channel->last_message_read = $firstMsgTs;
        $channel->save();
    }

    $conn->commit();
}
