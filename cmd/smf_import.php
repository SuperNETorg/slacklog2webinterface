<?php

require __DIR__ . '/../init.php';

echo date('[Y-m-d H:i]') . " import messages " . str_repeat('*', 20) . PHP_EOL;
$globalConfig = require __DIR__ . '/../config/global.php';
$smfConfig = require __DIR__ . '/../config/smf.php';

/** @var SMFApi_Abstract $smf */
$smf = SMFApi_Abstract::factory($smfConfig);

// Create new topics (if needed)
/** @var DB_Channel[] $channels */
$channels = DB_Channel::whereNull('smf_topic_id')->get();

foreach ($channels as $channel)
{
    // create new topic
    echo "Create net topic for channel #{$channel->id} '{$channel->name}'" . PHP_EOL;
    $topicId = postTopic($smf, $channel->name, $smfConfig['board_id']);

    // link topic id to channel
    $channel->smf_topic_id = $topicId;
    $channel->save();
}

// Post messages
$importMessages = new SMFImportMessages($smf, $smfConfig, new DateTimeZone($globalConfig['timezone']));
$channels = DB_Channel::all();
foreach ($channels as $channel)
{
    $count = $importMessages->run($channel);
    if ($count > 0)
    {
        echo "Exported $count messages from channel #{$channel->name}" . PHP_EOL;
    }
}

function postTopic(SMFApi_Abstract $api, $channelName, $boardId)
{
    $msgOptions = array(
        'subject' => $channelName,
        'body' => ''
    );
    $topicOptions = array(
        'board' => $boardId
    );
    $posterOptions = array();

    $result = $api->createPost($msgOptions, $topicOptions, $posterOptions);

    if (!$result)
    {
        echo "Post message fail" . PHP_EOL;
        exit(1);
    }

    return $result['topic']['id'];
}
