<?php
require __DIR__ . '/../vendor/autoload.php';
$dbconfig = include __DIR__ . '/../config/db.php';
$global = include __DIR__ . '/../config/global.php';
date_default_timezone_set($global['timezone']);
require __DIR__.'/../db.php';

mkdir("logs", 0777);

$slackdb = new SlackLogDatabase($dbconfig);
$channels = $slackdb->fetchChannels();

foreach($channels as $channel) {
    mkdir("logs/".$channel->name, 0777);

    $dates = $slackdb->fetchDates($channel->id);
    print("dumping channel: ".$channel->name);

    foreach ($dates as $date) {
        $date_format = $date->year.'-'.$date->month.'-'.$date->day;
        $messages = $slackdb->fetchMessagesByDate($channel->id, $date->year, $date->month, $date->day);

        $logfile = "logs/".$channel->name.'/'.$date_format.'.txt';
        $text = "";

        foreach($messages as $message) {
            $message_date = $message->message_date;
            $user_name = $message->user_name;
            $message_text = $message->message_text;
            $text .= "[$message_date] <$user_name> $message_text\n";
        }
        file_put_contents($logfile, $text);
    }
}