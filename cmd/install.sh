PASSWORD="<change me>"
TOKEN="<slack token here>"

# install necessary packages
# please set mysql password during installation
apt-get update
apt-get -y install apache2 php5-mysqlnd php5-json php5-curl php5-cli git libapache2-mod-php5 mysql-server

service mysql start

# clone webinterface project
mv /var/www/html /var/www/bkp
git clone https://bitbucket.org/SuperNETorg/slacklog2webinterface /var/www/html

# install php composer
cd /var/www/html
php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php
php -r "if (hash('SHA384', file_get_contents('composer-setup.php')) === 'fd26ce67e3b237fffd5e5544b45b0d92c41a4afe3e3f778e942e43ce6be197b9cdc7c251dcde6e2a52297ea269370680') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); }"
php composer-setup.php
php -r "unlink('composer-setup.php');"

# install project modules
php composer.phar install

# create database and slack user
tee create_slack_user.sql << ENDSQL
create database slack;
create user 'slack'@'localhost' identified by '$PASSWORD';
grant all privileges on slack.* to 'slack'@'localhost';
flush privileges;
ENDSQL

# use this to create slack DB (need to enter mysql root password into prompt)
mysql -u root -p < create_slack_user.sql

rm create_slack_user.sql -f

tee config/db.php << ENDCONFIG
<?php

return array(
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'slack',
    'username'  => 'slack',
    'password'  => '$PASSWORD',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
);
ENDCONFIG

tee config/slack.php << ENDSLACK
<?php

return array(
    'token' => '$TOKEN'
);
ENDSLACK

php5 cmd/db_setup.php 
 
tee .htaccess << ENDHTACCESS
RewriteEngine On
RewriteBase /

RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^ index.php [QSA,L]
ENDHTACCESS

a2enmod rewrite

tee /etc/apache2/sites-available/000-default.conf << ENDAPACHE
<VirtualHost *:80>
	#ServerName www.example.com
	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/html

	<Directory /var/www/html>
		Options Indexes FollowSymLinks MultiViews
		AllowOverride All
		Order allow,deny
		allow from all
	</Directory>

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
ENDAPACHE

chown www-data:www-data -R /var/www/html
chmod 770 -R /var/www/html

service apache2 restart

(crontab -l ; echo "0 */4 * * * php /var/www/html/cmd/fetch.php >> /var/log/slack_fetch.log 2>&1")| crontab -

