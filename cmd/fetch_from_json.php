<?php

require __DIR__ . '/../init.php';
define('JSON_DIR', __DIR__.'/../slack_json');

use Illuminate\Database\Capsule\Manager as DB;

$conn = DB::connection();

// Fetch users list
echo "collecting users list" . PHP_EOL;
$usersData = json_decode(file_get_contents(JSON_DIR.'/users.json'));

$conn->beginTransaction();
foreach ($usersData as $userData)
{
    /** @var DB_User $user */
    $user = DB_User::findOrNew($userData->id);
    $user->setRawAttributes(
        array(
            'id' => $userData->id,
            'name' => $userData->name,
            'deleted' => $userData->deleted ? 1 : 0,
            'real_name' => $userData->profile->real_name,
            'email' => @$userData->profile->email,
            'skype' => @$userData->profile->skype,
            'phone' => @$userData->profile->phone,
        )
    );
    $user->save();
}
$conn->commit();

echo "collecting channel list" . PHP_EOL;
$channelsData = json_decode(file_get_contents(JSON_DIR.'/channels.json'));
$conn->beginTransaction();
foreach ($channelsData as $channelData)
{
    /** @var DB_Channel $channel */
    $channel = DB_Channel::findOrNew($channelData->id);
    $channel->setRawAttributes(
        array(
            'id' => $channelData->id,
            'name' => $channelData->name,
        )
    );
    $channel->save();
}
$conn->commit();

echo "collecting channel messages" . PHP_EOL;
foreach ($channelsData as $channel)
{
    echo "\tChannel #{$channel->id} '{$channel->name}'" . PHP_EOL;

    $conn->beginTransaction();

    $files = scandir(JSON_DIR.'/'.$channel->name);
    $result = [];
    foreach ($files as $file) {
        $fname = JSON_DIR.'/'.$channel->name.'/'.$file;
        $messages = json_decode(file_get_contents($fname));

        foreach ($messages as $messageData) {
            if (isset($messageData->subtype)) { continue; }

            $msg = DB_Message::create([
                'channel' => $channel->id,
                'text' => $messageData->text,
                'ts' => $messageData->ts,
                'type' => $messageData->type,
                'user' => $messageData->user,
            ]);

        }
    }
    $conn->commit();
}
echo "done";
