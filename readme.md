# Requirements

* PHP >= 5.4
* PHP extensions: curl, mysql
* MySQL

# Installation

Logging works with either irclogbot (needs irc gateway) or using slack app (needs adding a new app to slack with the proper access rights).

## irclogbot installation
* login as a regular user
* run `python3 slacklog2webinterface/logger_bot/sopel.py` and create config file
* edit config file `/home/<botuser>/.sopel/logs/default.cfg` to look like this:
```text
[core]
nick = irclogbot
host = <slack irc gateway>
use_ssl = true
port = 6667
owner = irclogbot
auth_method = server
auth_password = <server password>
channels = <#channel1,#channel2>
```
* add this to logrotate:
```text
/home/<botuser>/.sopel/logs/raw.log {
    daily
    maxsize 10M
    compress
}
```

TODO: The bot reads new channels from the raw.log file. This is a little hacky and there must be a better way to pass irc server replies back to the joinall.py module.


## slack app token creation
* create a new app here: https://api.slack.com/applications/new
* fill info, set redirect url to your site (slacklogger for example)
* create app, save your client ID and client SECRET
* execute in shell:
```sh
export CLIENT_ID="<client id here>"
echo "https://slack.com/oauth/authorize?scope=users%3Aread+groups%3Aread+channels%3Aread+channels%3Ahistory&client_id=$CLIENT_ID"
```
* open the second result url
* authorize slack
* copy code=`something` part from redirected URL
* set CLIENT_ID, CLIENT_SECRET, CODE and execute curl:
```sh
export CLIENT_ID="<id>"
export CLIENT_SECRET="<secret>"
export CODE="<code>"
curl -G -v "https://slack.com/api/oauth.access" \
    --data-urlencode "client_id=$CLIENT_ID" \
    --data-urlencode "client_secret=$CLIENT_SECRET" \
    --data-urlencode "code=$CODE"
```
* copy token from json result

## install slack log reader

* set PASSWORD (for mysql) and TOKEN (for slack) in `cmd/install.sh`
* execute `cmd/install.sh`
    - install packages with apt (`apache, php5 modules, mysql, git`)
    - git clones slacklog2webinterface project to `/var/www/html`
    - installs php composer and necessary project modules
    - creates database, slack user, sets privileges
    - sets password in `config/db.php`
    - sets token in `config/slack.php`
    - creates tables with `cmd/db_setup.php`
    - adds `.htaccess` file to `/var/www/html` and enables apache2 rewrite module
    - enables apache2 rewrite module and writes `/etc/apache2/sites-available/000-default.conf` to enable `.htaccess`
    - sets /var/www/html permissions
    - restarts apache
    - adds cronjob for fetching messages every 4 hours (logs are in `/var/log/slack_fetch.log`

* IMPORTANT: `cmd/install.sh` needs mysql root password (first during apt-get install, second during mysql user+table creation)

* it is a good idea to delete `cmd/install.sh` after installation

# Old installation method

* Install packages via composer (`php composer.phar install`)
* Fill config files (config/*.php)
* Setup DB structure (run `php cmd/db_setup.php`)
* Add cron jobs:
```sh
    # Fetching new messages from Slack (example: every 4 hours)
    0 */4 * * * php /var/www/html/cmd/fetch.php >> /var/log/slack_fetch.log 2>&1
```

# Dumping channels from/to database

* `cmd/fetch.php` is the existing message downloader that uses Slack API
* `cmd/fetch_from_json.php` is used to read json files exported from Slack. 
    - Usage: extract slack.zip into slack_json, then run `php cmd/fetch_from_json.php`
    - this can be used to fill database with past logs on first setup
    - IMPORTANT: script shows a lot of warnings but inserts the data to mysql
* `cmd/dump_logs.php` dumps the logs from MySQL to plain text files.
