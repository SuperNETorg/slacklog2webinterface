<?php

class SlackLogDatabase {
    function __construct($dbconfig) {
        # $this->dbh = new PDO("mysql:host=".$dbconfig['host'].";dbname=".$dbconfig['database'], $dbconfig['username'], $dbconfig['password']);
        # $db = new PDO('sqlite:dogsDb_PDO.sqlite');
        $this->dbh = new Illuminate\Database\Capsule\Manager;
        $this->dbh->addConnection($dbconfig);
        $this->dbh->setAsGlobal();
        $this->dbh->bootEloquent();
    }
    
    public function query($query, $vars) {
        $result = $this->dbh->getConnection()->select($query, $vars);
        return $result;
    }

    public function fetchChannels() {
        return $this->query("select * from channels",[]);
    }

    public function fetchChannel($channel_id) {
        $channel = $this->query("select * from channels where id=?", [$channel_id]);
        if ($channel) {
            return $channel[0];
        } else {
            return false;
        }
    }

    public function fetchDates($channel_id) {
        return $this->query("SELECT
                              year(from_unixtime(ts))  AS year,
                              month(from_unixtime(ts)) AS month,
                              day(from_unixtime(ts))   AS day
                            FROM messages
                            where channel=?
                            group by year, month, day
                            ORDER BY year, month, day", [$channel_id]);
    }

    public function fetchMessagesByDate($channel_id, $year, $month, $day) {
        return $this->query("select
                              users.name as user_name,
                              messages.text as message_text,
                              from_unixtime(messages.ts) as message_date
                            from messages
                            join users on messages.user=users.id
                            where
                                  messages.channel=?
                              and (    year(from_unixtime(messages.ts))=?
                                   and month(from_unixtime(messages.ts))=?
                                   and day(from_unixtime(messages.ts))=?
                                  )",
                            [$channel_id, $year, $month, $day]);
    }

    public function searchGlobal($query) {
        return $this->query("select
                              users.name as user_name,
                              messages.text as message_text,
                              from_unixtime(messages.ts) as message_date,
                              channels.name as message_channel
                            from messages
                            join users on messages.user=users.id
                            join channels on messages.channel=channels.id
                            where messages.text like ?",
                            [$query]);
    }

    public function searchChannel($channel_id, $query) {
        return $this->query("select
                              users.name as user_name,
                              messages.text as message_text,
                              from_unixtime(messages.ts) as message_date
                            from messages
                            join users on messages.user=users.id
                            where     messages.channel=?
                                  and messages.text like ?", [$channel_id, $query]);
    }
}
