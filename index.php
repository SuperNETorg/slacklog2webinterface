<?php
require __DIR__ . '/vendor/autoload.php';
$dbconfig = include __DIR__ . '/config/db.php';
$global = include __DIR__ . '/config/global.php';
date_default_timezone_set($global['timezone']);

require __DIR__.'/db.php';

$loader = new Twig_Loader_Filesystem(__DIR__.'/templates');
$twig = new Twig_Environment($loader /* ,array(twig config) */);

$slackdb = new SlackLogDatabase($dbconfig);

$app = new Slim\App([
    'debug' => true,
    'log.enabled' => true
]);

$app->get('/', function (Slim\Http\Request $request, Slim\Http\Response $response) use ($twig, $slackdb) {
    $allchannels = $slackdb->fetchChannels();
    $firstchannel = $allchannels[0];
    $restchannels = array_slice($allchannels, 1);


    $dates = $slackdb->fetchDates($firstchannel->id);

    $vars = [
        'restchannels' => $restchannels,
        'firstchannel' => $firstchannel,
        'dates' => $dates
    ];
    $response->write($twig->render('index.html', $vars));
    return $response;
});

$app->get('/channel', function (Slim\Http\Request $request, Slim\Http\Response $response) use ($twig, $slackdb) {
    $channel_id = $request->getParam('channel_id');
    $channel = $slackdb->fetchChannel($channel_id);
    $dates = $slackdb->fetchDates($channel_id);

    $vars = [
        'channel' => $channel,
        'dates' => $dates
    ];

    $response->write($twig->render('dates.html', $vars));
    return $response;
});

$app->get('/logs', function (Slim\Http\Request $request, Slim\Http\Response $response) use ($twig, $slackdb) {
    $channel_id = $request->getParam("channel_id");
    $year = $request->getParam('year');
    $month = $request->getParam('month');
    $day = $request->getParam('day');

    $channel = $slackdb->fetchChannel($channel_id);
    $messages = $slackdb->fetchMessagesByDate($channel_id, $year, $month, $day);

    $vars = [
        'channel' => $channel,
        'messages' => $messages,
        'date' => [
            'year' => $year,
            'month' => $month,
            'day' => $day
        ],
        'date_format' => "Y-m-d"
    ];

    $response->write($twig->render('messages.html', $vars));
    return $response;
});

$app->get('/search', function (Slim\Http\Request $request, Slim\Http\Response $response) use ($twig, $slackdb) {
    $q = $request->getParam("q");
    $messages = $slackdb->searchGlobal('%'.$q.'%');

    $vars = [
        'q' => $q,
        'messages' => $messages,
        'empty' => ($messages == []) ? true : false
    ];

    $response->write($twig->render('global_search_results.html', $vars));
    return $response;
});

$app->get('/search/{channel}', function (Slim\Http\Request $request, Slim\Http\Response $response) use ($twig, $slackdb) {
    $channel_id = $request->getAttribute('channel');
    $q = $request->getParam("q");

    $channel = $slackdb->fetchChannel($channel_id);
    $messages = $slackdb->searchChannel($channel_id, '%'.$q.'%');

    $vars = [
        'q' => $q,
        'channel' => $channel,
        'messages' => $messages,
        'empty' => ($messages == []) ? true : false
    ];

    $response->write($twig->render('channel_search_results.html', $vars));
    return $response;
});


$app->run();
