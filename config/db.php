<?php

return array(
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'slack',
    'username'  => 'slack',
    'password'  => 'changeme',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
);

